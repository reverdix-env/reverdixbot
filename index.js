#!/bin/node
/*
Copyright (C) <2019>  <scareLOL444>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

you can find the complete source at <https://gitlab.com/reverdix-env/reverdixbot/>
for further information, you can contact me at <scareLOL444@outlook.com>
*/

console.log("loading config");
const config = require("./config/config.js");
console.log("loading logging module");
const logging = require(config.libs + "logging.js");
logging.logFolder = config.logFolder
logging.logging = config.log;
logging.debugMode = config.debug;
if (config.logAll) {
	logging.log("logAll is activated, the bot will log every message in the chat, wether it is a command or not", logging.warning);
}
if (config.debug) {
	logging.log("debug mode is activated, this feature is only intended for developpers, it is not recomended for normal use", logging.warning);
}
logging.log("loading tmi");
const tmi = require("tmi.js");
logging.log("loading client config");
const clientConfig = require(config.config + "clientConfig.js");
logging.log("loading store module");
const store = require(config.modules + "store.js");
logging.log("loading user module");
const user = require(config.modules + "user.js");

logging.log("loading admin module");
const admin = require(config.modules + "admin.js");
//logging.log("initializing admin module", logging.debug);
//var dac = admin.initAdmins(config.defAdmins);
//logging.log("granted admin permission to " + dac + " users", logging.debug);

logging.log("loading webpost module")
const webpost = require(config.modules + "webpost.js");
logging.log("loading rand library");
const rand = require(config.libs + "rand.js");
logging.log("loading coupon library");
const coupon = require(config.libs + "coupon.js");

const chat = clientConfig.channel;
//Connect to twitch server
logging.log("connecting to server");
const client = new tmi.client(clientConfig);
client.connect();
logging.log("");
logging.log("");
logging.log("START BOT");
logging.log("");

var currentUsers = [];//Registered viewers (has no link to the user database)

function isRegistered(username){//Checks if a user is registered as a viewer
	for (let i = 0; i < currentUsers.length; i++) {
		if (currentUsers[i] == username){
			return true;
		}
	}
	return false;
}

function debugLog(message) {
	if (config.debug) {
		logging.log(message, logging.debug);
	}
}

logging.log("registering events");
client.on("chat", (channel, userstate, message, self) => {
	if (self) return; //Ignore messages from the bot itself

	//database and active list registration
	user.updateInfo(userstate, false, config.defaultBaloches, config.data + "user.json")
	if (!isRegistered(userstate.username)){
		debugLog("user was not registered");
		currentUsers.push(userstate.username);
	}

	//parsing message
    if (message.substr(0, 1) == "!" && config.active){ //only work on messages with the prefix
	if (message == "!!!active" && userstate.username == "scarelol444") {
	    logging.log(userstate.username + " deactivated the bot");
            config.active = false;
            return;
	}
	var msg = message.toLowerCase().split(" "); //separate words
	logging.log(userstate.username + " ran command " + message);

	switch (msg[0]){
	    
	case "!ping": //!ping command
		client.say(chat, `@${userstate.username} pong!`);
	    break;
	    
	case "!store": //!store [object] command
	    if (msg.length < 2){//If there is no [object]
			client.say(channel, "Objets disponibles dans le magasin: " + store.getFinderStr(config.data + "store.json"));
	    }
		else{
			//If there is [object]
			if (store.isItemAvailable(msg[1], config.data + "store.json")){//If the item is available

				//Get user and item data
				var us = user.getUserFromId(userstate["user-id"], config.data + "user.json");
				var item = store.findItem(msg[1], config.data + "store.json");

				if (us.baloches >= item.price){
					//update user balance and buy timeout
					user.addBaloches(userstate["user-id"], -item.price, config.data + "user.json");
					store.updateItemTime(msg[1], config.data + "store.json");

					client.say(channel, "@" + userstate.username + " a achete " + item.label + " pour " + item.price);

					//webpost
					var web = webpost.newPost(config.web, item.finder, {name:us.username, ammount:item.price, label:item.label, description:item.description});
					if (web < 0) {
						logging.log("an unexpected error occured while attempting to post a bought item to the web interface: " + item.finder + " ; " + JSON.stringify({name:us.username, ammount:item.price, label:item.label, description:item.description}) + " ; a description of the error will follow", logging.warning);
						switch (web) {
							case -1:
								logging.log("the web store list file is inexistant", logging.warning);
								break;
							case -2:
								logging.log("an error occured while attempting to read the web store list file", logging.warning);
								break;
							case -3:
								logging.log("the data within the web store list file is corrupted", logging.warning);
								break;
							case -4:
								logging.log("unable to update the web store list", logging.warning);
								break;
							case -5:
								logging.log("an error occured while attempting to write the web store list file", logging.warning);
								break;
							default:
								logging.log("an unknown error occured, code: " + web, logging.warning);
								break;
						}
						logging.log("attempting to restore the web store list file", logging.warning);
						var webError = webpost.clearPosts(config.web);
						if(webError == null) {
							logging.log("web store list file restored successfuly, performing a second attempt");
							var web2 = webpost.newPost(config.web, item.finder, {name:us.username, ammount:item.price, label:item.label, description:item.description});
							if (web2 >= 0) {
								logging.log("web store list file update successful on second attempt");
								break;
							}
							logging.log("second attempt failed, code: " + web2, logging.error);
						}
						logging.log("unable to reset the web store list file !", logging.error);
						client.say(channel, "Oops, j'ai eu un probleme pendant la mise a jour du magasin. Veuillez transmettre la date et l'heure du probleme au responsable afin de faciliter son travail. Je vais desormais planter.");
						logging.log("the bot will now crash !", logging.error);
						logging.log(webError, logging.error);
						throw webError;
					}
					else {
						logging.log("web store list file update successfull");
					}
				}
				else{//If not enough money
					client.say(channel, item.label + " coute " + item.price + " mais vous avez seulement " + us.baloches);
				}
			}
			else{
				//If the item is unavailable
				var item = store.findItem(msg[1], config.data + "store.json");
				if (item == null){
					client.say(channel, "Objet invalide, vous pouvez obtenir la liste des objets disponibles avec la commande '!store'");
				}
				else{
					var t = new Date();
					var tt = new Date(item.last);
					client.say(channel, item.label + " n'est pas encore disponible, il le sera dans " + (item.cooldown - Math.round((t - tt) / 1000)) + "s");
				}
			}
	    }
	    break;
	
	case "!storeinfo":
		if (msg[1] == undefined) {
			client.say(channel, "Specifiez un objet du magasin pour obtenir plus d'information dessus.");
			break;
		}

		var item = store.findItem(msg[1], config.data + "store.json");
		if (item == null) {
			client.say(channel, "Objet invalide.");
			break;
		}

		client.say(channel, item.label + "; description: " + item.description + "; prix: " + item.price + "; delai d'attente: " + item.cooldown + "s");
		break;

	case "!help": //!help command
		if (msg[1] == undefined) {
			client.say(chat, "Les commandes suivantes sont disponibles: '!ping'; '!store'; '!storeinfo'; '!baloches'; '!coupon'; '!flip'; '!roll'; '!help'; '!source'; '!info'; vous pouvez faire '!help [command]' pour plus de details sur une commande");
		}
		else {
			switch (msg[1]) {
				case "!ping":
					client.say(channel, "Pong!");
					break;

				case "!store":
					client.say(channel, "Utilise seul pour obtenir la liste des objets du magasin, ou avec un argument ('!store [objet]') pour acheter un objet");
					break;

				case "!storeinfo":
					client.say(channel, "Donne des infos detaillees sur un objet du magasin ('!storeinfo [objet]')")
					break;

				case "!baloches":
					client.say(channel, "Pour se la peter avec ses richesses");
					break;

				case "!coupon":
					client.say(channel, "Permet de recuperer les baloches associees a un coupon ('!coupon [coupon]')");
					break;

				case "!flip":
					client.say(channel, "Lancez une piece pour doubler la mise ('!flip [ammount]')");
					break;

				case "!roll":
					client.say(channel, "Lancez un de pour tripler la mise ('!roll [ammount]')");
					break;

				case "!help":
					client.say(channel, "Visiblement tu la connais celle la ;)");
					break;
				
				case "!source":
					client.say(channel, "Gives a link to this bot's source repository");
					break;

				case "!info":
					client.say(channel, "Donne des informations generales a propos du bot");
					break;

				default:
					client.say(channel, "Cette commande n'existe pas (le ! est inclu dans le nom de la commande)");
					break;
			}
		}
		
		break;
	
	case "!baloches": //!baloches command
		var us = user.getUserFromId(userstate["user-id"], config.data + "user.json");
		client.say(channel, "@" + userstate.username + " a actuellement " + us.baloches + " baloches");
		break;

	case "!flip": //!flip command
		var us = user.getUserFromId(userstate["user-id"], config.data + "user.json");
		var bet = parseInt(msg[1], 10);
		var bal = us.baloches;
		var valid = true;
		if (msg[1] == undefined)
			valid = false;
		for (var c in msg[1]) {
			if (msg[1][c] != '0' && msg[1][c] != '1' && msg[1][c] != '2' && msg[1][c] != '3' && msg[1][c] != '4' &&
			msg[1][c] != '5' && msg[1][c] != '6' && msg[1][c] != '7' && msg[1][c] != '8' && msg[1][c] != '9') {
				valid = false;
			}
			logging.log("User tried to bet '" + msg[1] + "', char '" + msg[1][c] + "'", logging.debug)
		}
		if (msg[1] == "all") {
			bet = us.baloches;
			valid = true;
		}
		if (!valid) {
			client.say(channel, "@" + userstate.username + " entree invalide, merci de n'utiliser que des nombres");
			break;
		}
		if (bal < bet) {
			client.say(channel, "@" + userstate.username + " desole mais vous n'avez que " + bal + " baloches");
			break;
		}
		if (bet < config.minimumBet) {
			client.say(channel, "@" + userstate.username + " le pari minimum est de " + config.minimumBet);
		}
		else {
			var ran = rand.randInt(0, 1000);
			if (ran < 500) {
				client.say(channel, "@" + userstate.username + " felicitation, vous avez gagne " + bet + " baloches");
				user.addBaloches(userstate["user-id"], bet, config.data + "user.json");
			}
			else {
				client.say(channel, "@" + userstate.username + " desole, vous avez perdu " + bet + " baloches");
				user.addBaloches(userstate["user-id"], -bet, config.data + "user.json");
			}
		}
		break;

	case "!roll": //!roll command
		var us = user.getUserFromId(userstate["user-id"], config.data + "user.json");
		var bet = parseInt(msg[1], 10);
		var bal = us.baloches;
		var valid = true;
		if (msg[1] == undefined)
			valid = false;
		for (var c in msg[1]) {
			if (msg[1][c] != '0' && msg[1][c] != '1' && msg[1][c] != '2' && msg[1][c] != '3' && msg[1][c] != '4' &&
			msg[1][c] != '5' && msg[1][c] != '6' && msg[1][c] != '7' && msg[1][c] != '8' && msg[1][c] != '9') {
				valid = false;
			}
		}
		if (msg[1] == "all") {
			bet = us.baloches;
			valid = true;
		}
		if (!valid) {
			client.say(channel, "@" + userstate.username + " entree invalide, merci de n'utiliser que des nombres");
			break;
		}
		if (bal < bet) {
			client.say(channel, "@" + userstate.username + " desole mais vous n'avez que " + bal + " baloches");
			break;
		}
		if (bet < config.minimumBet) {
			client.say(channel, "@" + userstate.username + " le pari minimum est de " + config.minimumBet);
		}
		else {
			var ran = rand.randInt(0, 3000);
			if (ran < 1000) {
				client.say(channel, "@" + userstate.username + " felicitation, vous avez gagne " + (bet * 2) + " baloches");
				user.addBaloches(userstate["user-id"], bet * 2, config.data + "user.json");
			}
			else {
				client.say(channel, "@" + userstate.username + " desole, vous avez perdu " + bet + " baloches");
				user.addBaloches(userstate["user-id"], bet * -1, config.data + "user.json");
			}
		}
		break;
	
	case "!source": //!source command
		client.say(channel, "You can find this bot's source at <https://gitlab.com/reverdix-env/reverdixbot/>");
		break;

	case "!coupon": //!coupon command
		if (msg[1] == undefined) {
			client.say(channel, "Merci de specifier un coupon (ils suivent generalement ce format: '0123-4567-89AB-CDEF')");
			break;
		}
		var couponValue = coupon.redeemCoupon(config.shared + "coupons.json", msg[1].toUpperCase());
		if (couponValue == -1) {
			client.say(channel, "coupon invalide (ils suivent generalement ce format: '0123-4567-89AB-CDEF')");
			logging.log("user " + userstate.username + ", id: " + userstate["user-id"] + ", tried to redeem the following coupon: " + msg[1].toUpperCase(), logging.warning);
			break;
		}
		user.addBaloches(userstate["user-id"], couponValue, config.data + "user.json");
		client.say(channel, "Felicitation @" + userstate.username + ", vous avez recupere vos " + couponValue + " baloches !");
		logging.log("user " + userstate.username + ", id: " + userstate["user-id"] + ", redeemed the following coupon: " + msg[1].toUpperCase() + " for " + couponValue);
		break;

	case "!info": //!info command
		client.say(channel, "infos sur le bot: nom: '" + clientConfig.identity.username + "'; salon actuel: '#" + clientConfig.channel + "'; version: '" + config.version + "'");
		break;
	}
    }
    else{
        if (message == "!!!active" && userstate.username == "scarelol444" && !config.active) {
            logging.log(userstate.username + " reactivated the bot");
            config.active = true;
	    return;
	}
		if (config.logAll) {
			logging.log(userstate.username + ": " + message, "logAll");
		}
    }
});

client.on("join", (channel, username, self) => {//Register joining viewers
	if (self) return;
	if (!isRegistered(username)){
		currentUsers.push(username);
	}
});

client.on("part", (channel, username, self) => {//Unregister leaving viewers
	if (self) return;
	if (isRegistered(username)){
		for (let i = 0; i < currentUsers.length; i++) {
			if (currentUsers[i] == username){
				currentUsers.splice(i, 1);
			}
		}
	}
});

//Regular messages
setInterval( ()=> {
//    client.say(clientConfig.channel, "Faites '!help' pour voir les commandes disponibles");
}, config.regularHelpInterval);

setInterval( ()=> {//Every 60 seconds, add 10 baloches to everyone
	logging.log("regular payout");
	for (let i = 0; i < currentUsers.length; i++) {//Add 10 baloches to everyone registered and in the database
		var us = user.getUserFromName(currentUsers[i], config.data + "user.json");
		if (us != null) {
			user.addBaloches(us.id, config.regularGain, config.data + "user.json");
			logging.log("added baloches for " + currentUsers[i] + " " + us.baloches, logging.debug);
		}
	};
}, config.regularGainInterval);
