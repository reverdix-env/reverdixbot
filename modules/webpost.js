/*
Copyright (C) <2019>  <scareLOL444>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

you can find the complete source at <https://gitlab.com/reverdix-env/reverdixbot/>
for further information, you can contact me at <scareLOL444@outlook.com>
*/

const fs = require("fs")
const xml = require('xml-js');

const webpost = {

    createXml: function(data) {
        var rawXml = xml.js2xml(data, {compact: true, spaces: 4});
        var rawXml = "<webStore>\n" + rawXml + "\n</webStore>";
        return rawXml;
    },

    readXml: function(data) {
        //console.log("reading");
        //console.log(data.webStore);
        data = data.webStore;
        data.number = parseInt(data.number["_text"], 10);
        return data;
    },

    newPost: function(path, title, content) {
        if (!fs.existsSync(path + "storeList.json")) {
            return -1;
        }
        var posts;
        try {
            //posts = JSON.parse(fs.readFileSync(path + "storeList.json", "utf8"), null, 2);
            posts = xml.xml2js(fs.readFileSync(path + "storeList.xml", "utf8"), {compact: true, spaces: 4});
            //console.log("xml");
            //console.log(posts);
            posts = this.readXml(posts);
            //console.log("xml2");
            //console.log(posts);
        }
        catch(error) {
            console.log(error)
            return -2;
        }
        if (posts.number == undefined || posts.number == null || posts.number < 0 || posts.list == undefined || posts.list == null || Object.keys(posts.list).length != posts.number) {
            //console.log(posts.number == undefined);
            //console.log(posts.number == null)
            return -3
        }
        try {
            posts.list["n" + posts.number.toString(10)] = {
                number: posts.number,
                title: title,
                content: content
            }
            posts.number++;
        }
        catch(error) {
            return -4
        }
        try {
            //fs.writeFileSync(path + "storeList.json", JSON.stringify(posts, null, 2), "utf8");
            fs.writeFileSync(path + "storeList.xml", this.createXml(posts));
        }
        catch(error) {
            return -5;
        }
        return 0;
    },

    clearPosts: function(path) {
        try {
            //fs.writeFileSync(path + "storeList.json", JSON.stringify({number: 0, list: []}, null, 2), "utf8");
            fs.writeFileSync(path + "storeList.xml", this.createXml({number: 0, list: ['']}));
            return null;
        }
        catch(error) {
            return error;
        }
    }
}

module.exports = webpost;