/*
Copyright (C) <2019>  <scareLOL444>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

you can find the complete source at <https://gitlab.com/reverdix-env/reverdixbot/>
for further information, you can contact me at <scareLOL444@outlook.com>
*/

const admin = {
    admins: [],

    initAdmins: function (defAdmins) {
        var defNum = 0;
        defAdmins.forEach(element => {
            admins.append(element);
            defNum++;
        });
        return defNum;
    },

    addAdmin: function (name) {
        if (!isAdmin(name)) {
            admins.splice(0, 0, name);
        }
    },

    delAdmin: function (name) {
        if (isAdmin(name)) {
            admins.splice(admins.indexOf('foo'), 1);
        }
    },

    isAdmin: function (name) {
        return admins.include(name);
    },

    isMod: function (userstate) {

    },

    hasPerms: function (userstate) {
        return isMod(userstate) || isAdmin(userstate.username)
    }
}