/*
Copyright (C) <2019>  <scareLOL444>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

you can find the complete source at <https://gitlab.com/reverdix-env/reverdixbot/>
for further information, you can contact me at <scareLOL444@outlook.com>
*/

const fs = require("fs");

const store = {
	
	loadStore: function (path, reviver, format, replacer, space) {
		if (replacer == undefined)
			replacer = null;
		if (space == undefined)
			space = 4;
		if (format == undefined)
			format = "utf8";
		try {
			return JSON.parse(fs.readFileSync(path, format), reviver);
		}
		catch(error) {
			return null;
		}
	},

	saveStore: function (path, store, replacer, space, format) {
        if (replacer == undefined)
            replacer = null;
        if (space == undefined)
            space = 4;
        if (format == undefined)
            format = "utf8";
        try {
            fs.writeFileSync(path, JSON.stringify(store, replacer, space), format);
            return true;
        }
        catch(error) {
            return false;
        }
	},

	findItem: function (finder, path, reviver, format, replacer, space) {//Returns the store item with the corresponding finder, if nothing is found returns null or an error occurs
		var storeItems = this.loadStore(path, reviver, format, replacer, space);
		if (storeItems == null)
			return null;
		if (storeItems[finder] != undefined && storeItems[finder] != null)
			return storeItems[finder];
		return null;
	},

	isItemAvailable: function (item, path, reviver, format, replacer, space) {//if item is a string it will be treated as the finder, if not it will be treated as the item object
		var element;
		if (typeof item == "string")
			element = this.findItem(item, path, reviver, format, replacer, space);
		else
			element = item;
		if (element == null || element == undefined)
			return false;
		var t = new Date();
		var i = new Date(element.last);
		return  Math.round((t - i) / 1000) > element.cooldown;
	},

	updateItemTime: function (item, path, reviver, format, replacer, space) {
		var storeItems = this.loadStore(path, reviver, format, replacer, space);
		if (storeItems == null)
			return null;
		storeItems[item].last = new Date();
		if (this.saveStore(path, storeItems, replacer, space, format))
            return true;
        return false;
	},

	getFinderStr: function (path, reviver, format, replacer, space){//Returns a string containing the concatenation of each store item's finder
		var storeItems = this.loadStore(path, reviver, format, replacer, space);
		if (storeItems == null)
			return null;
		var list = "";
		for (const element in storeItems) {
			list+=storeItems[element].finder + " ; ";
		}
		return list
	}
}

module.exports = store;