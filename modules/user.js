/*
Copyright (C) <2019>  <scareLOL444>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

you can find the complete source at <https://gitlab.com/reverdix-env/reverdixbot/>
for further information, you can contact me at <scareLOL444@outlook.com>
*/

const fs = require("fs");

const user = {

    loadUsers: function (path, reviver, format, replacer, space) {
		if (replacer == undefined)
			replacer = null;
		if (space == undefined)
			space = 4;
		if (format == undefined)
			format = "utf8";
		try {
			return JSON.parse(fs.readFileSync(path, format), reviver);
		}
		catch(error) {
			return null;
		}
	},

	saveUsers: function (path, users, replacer, space, format) {
        if (replacer == undefined)
            replacer = null;
        if (space == undefined)
            space = 4;
        if (format == undefined)
            format = "utf8";
        try {
            fs.writeFileSync(path, JSON.stringify(users, replacer, space), format);
            return true;
        }
        catch(error) {
            return false;
        }
    },
    
    getUserFromName: function(name, path, reviver, format, replacer, space) {//Gets the users table id of the user from its name (when possible, prefer getUserFromId)
        var users = this.loadUsers(path, reviver, format, replacer, space);
        if(users == null)
            return null;
        for (var userid in users) {
            if (users[userid].username == name.toLowerCase()){
                return users[userid];
            }
        }
        return null;
    },

    getUserFromId: function(id, path, reviver, format, replacer, space) {
        var users = this.loadUsers(path, reviver, format, replacer, space);
        if(users == null)
            return null;
        if(users[id] != undefined) {
            return users[id];
        }
        return null;
    },

    createUser: function(userstate, defaultBaloches, overwrite, path, reviver, format, replacer, space) {//Creates a new user
        if (overwrite == undefined)
            overwrite = false;
        var users = this.loadUsers(path, reviver, format, replacer, space);
        if (users == null || ((users[userstate["user-id"]] != undefined || users[userstate["user-id"]] != null) && !overwrite))
            return false;
        users[userstate["user-id"]] = {
            id: userstate["user-id"],
            baloches: defaultBaloches,
            username: userstate.username.toLowerCase()
        };
        if (this.saveUsers(path, users, replacer, space, format))
            return true;
        return false;
    },

    addBaloches: function(id, ammount, path, reviver, format, replacer, space) {//adds baloches to a user (can remove baloches if ammount is negative)
        var users = this.loadUsers(path, reviver, format, replacer, space);
        if (users == null || users[id] == null || users[id] == undefined)
            return false;
        users[id].baloches += ammount;
        if (this.saveUsers(path, users, replacer, space, format))
            return true;
        return false;
    },

    updateInfo: function(userstate, reset, defaultBaloches, path, reviver, format, replacer, space) {
        var users = this.loadUsers(path, reviver, format, replacer, space);
        if (users == null)
            return false;
        if (users[userstate["user-id"]] == undefined || users[userstate["user-id"]] == null || reset) {
            return this.createUser(userstate, defaultBaloches, true, path);
        }
        users[userstate["user-id"]].username = userstate.username.toLowerCase();
        return this.saveUsers(path, users, replacer, space, format);
    }
}

module.exports = user;
