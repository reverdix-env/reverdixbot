//les valeures dans ce fichier peuvent etre modifiees a loisir afin d'ajuster le comportement du bot
//merci de suivre le formatage et les limites indiquees par les commentaires (lignes avec // au debut)

//formatages clasiques:
//  nombres: doit rester un nombre, doit rester dans l'intervale precise au cas par cas
//  booleens: soit true pour vrai soit false pour faux
//  chainnes de caracteres: dois toujours rester entre " attention a certains caracteres (notament " et \) qui peuvent casser la chainne

const config = {
//valeurs concerant le fonctionnement du bot

    //quantite de baloches donnee aux nouveaux utilisateurs
    defaultBaloches: 100,

    //valeur minimum d'un pari lors de l'utilisation
    minimumBet: 5,

    //gain regulier de viewer
    regularGain: 10,

    //interval de gain regulier (miliseconde)
    regularGainInterval: 300000,

    //interval de l'affichage du message d'aide
    regularHelpInterval: 600000,

//logging

    //activer l'enregistrement des logs (passer cette valeur a false desactive le logAll) (il est hautement decourage de le desactiver)
    log: true,

    //enregistrer tous les messages passant sur le chat (meme si ce n'est pas une commande)
    logAll: true,

    //logs de debug
    debug: true,

//administration

    //liste d'utilisateurs qui ont les privileges d'admins sur le bot par default (quelque soit leur status par rapport a la chaine)
    defAdmins: ["scarelol444"],

// /!\ attention: les options a partir d'ici affectent directement le comportement du bot et des erreur peuvent causer des disfonctionnements severes

//chemins d'acces

    //chemin d'access des librairies custom
    libs: "./libs/",

    //chemin d'access des modules complementaires
    modules: "./modules/",

    //chemin d'access des fichiers de donnees
    data: "./data/",

    //chemin d'acces de la config
    config: "./config/",

    //chemin d'acces de la page web de communication
    web: "../shared/",

    //dossier de stoquage des logs
    logFolder: "./log/",

    //dossier partage avec bot discord
    shared: "../shared/",

//etat actuel du bot (modifier uniquement avec les mises a jour)
    version: "v0.3.1",

//activite visible du bot (si false le bot continue de fonctionner mais ne reagit plus aux messages)
    active: true
}

module.exports = config;
