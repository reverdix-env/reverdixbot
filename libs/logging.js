/*
Copyright 2019 scareLOL444

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

const fs = require("fs");

const logging = {
    logFolder: "./",
    logging: true,
    debugMode: false,
    info: "info",
    warning: "warning",
    error: "error",
    debug: "debug",

    log: function (message, type) {
        if (!this.logging && type != this.error || (type == this.debug && !this.debugMode)) {
            return;
        }
        if (type == undefined) {
            type = this.info;
        }
        var t = new Date();
        message = "[" + t.getHours() + ":" + t.getMinutes() + ":" + t.getSeconds() +"] " + type + ": " + message;
        console.log(message);
        message += "\n"
        var file = this.logFolder + "log_" + (t.getDate()) + "-" + (t.getMonth() + 1) + "_.log";
        if (!fs.existsSync(file)) {
            fs.writeFileSync(file, "BEGIN LOG FILE\n");
        }
        fs.appendFileSync(file, message);
    }
}

module.exports = logging;