const rand = {
    randInt: function (min, max) {
        return Math.floor(Math.random() * Math.floor(max - min)) + min;
    }
}

module.exports = rand;
