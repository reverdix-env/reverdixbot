/*
Copyright 2019 scareLOL444

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

const fs = require("fs");

const coupon = {
    chars: ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'], //available characters for coupons, as it is the default value for a corrupted argument in a randCoupon() call, it is strongly recommended to keep this property in a working state

    charsOpt: { //pre furnished character tables for the random generator
        nums: ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'],
        chars: ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'U', 'V', 'W', 'X', 'Y', 'Z'],
        bin: ['0', '1'],
        octal: ['0', '1', '2', '3', '4', '5', '6', '7'],
        hex: ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F']
    },

    randInt: function (min, max) { //default random number function used, if modified, please keep its signature (randint(min:int, max:int):int) in mind or other functions unsing it might encounter issues
        return Math.floor(Math.random() * Math.floor(max - min)) + min;
    },
    
    //local work functions
    randCoupon: function (chars) { //generate a coupon code
        if (chars == undefined || chars == null)
            chars = this.chars;
        var coupon = chars[this.randInt(0, chars.length)] + chars[this.randInt(0, chars.length)] + chars[this.randInt(0, chars.length)] + chars[this.randInt(0, chars.length)];
        coupon += "-";
        coupon += chars[this.randInt(0, chars.length)] + chars[this.randInt(0, chars.length)] + chars[this.randInt(0, chars.length)] + chars[this.randInt(0, chars.length)];
        coupon += "-";
        coupon += chars[this.randInt(0, chars.length)] + chars[this.randInt(0, chars.length)] + chars[this.randInt(0, chars.length)] + chars[this.randInt(0, chars.length)];
        coupon += "-";
        coupon += chars[this.randInt(0, chars.length)] + chars[this.randInt(0, chars.length)] + chars[this.randInt(0, chars.length)] + chars[this.randInt(0, chars.length)];
        return coupon;
    },

    loadCoupons: function (path, reviver, format, replacer, space) { //load the coupon file, returns null in case of an issue
        if (replacer == undefined)
            replacer = null;
        if (space == undefined)
            space = 2;
        if (format == undefined)
            format = "utf8";
        try {
            return JSON.parse(fs.readFileSync(path, format), reviver);
        }
        catch(error) {
            return null;
        }
    },

    saveCoupons: function (path, coupons, replacer, space, format) { //save the coupon file, returns true on success, false on error
        if (replacer == undefined)
            replacer = null;
        if (space == undefined)
            space = 2;
        if (format == undefined)
            format = "utf8";
        try {
            fs.writeFileSync(path, JSON.stringify(coupons, replacer, space), format);
            return true;
        }
        catch(error) {
            return false;
        }
    },

    //user functions
    newCoupon: function (path, ammount, chars, overwrite, code, generator, replacer, space, format) { //creates a new coupon, returns -1 in case of issue
        if (generator == undefined)
            generator = this.randCoupon;
        if (chars == undefined)
            chars = this.chars
        var coupons = this.loadCoupons(path, replacer, space, format);
        if (ammount <= 0 || coupons == null || coupons == undefined)
            return -1;
        if (code == undefined) {
            code = generator(chars);
            while (!overwrite && !coupons[code.toUpperCase()] == undefined) {
                code = generator(chars);
            }
        }
        else if (!overwrite && coupons[code.toUpperCase()] != undefined)
            return -1;
        coupons[code.toUpperCase()] = ammount;
        var s = this.saveCoupons (path, coupons, replacer, space, format);
        if (!s)
            return -1;
        return code;
    },

    redeemCoupon: function (path, coupon, keep, replacer, space, format) { //redeem the ammount of a coupon, returns -1 in case of issue
        if (coupon == undefined || coupon == null)
            return -1;
        if (keep == undefined)
            keep = false;
        var coupons = this.loadCoupons(path, replacer, space, format);
        if (coupons == null || coupons == undefined)
            return -1;
        var ammount = coupons[coupon.toUpperCase()];
        if (ammount == undefined)
            return -1;
        if (!keep)
            delete coupons[coupon.toUpperCase()];
        var s = this.saveCoupons (path, coupons, replacer, space, format);
        if (!s)
            return -1;
        return ammount;
    }
}

module.exports = coupon;

/*
documentation:

chars:table
this table is used as default when a call to randCoupon has no argument
it is strongly recommended to keep this property in a working state

charsOpt:object
this object contains lists of characters that can be used for random coupon generation
this object is only here to ease modularity and is not necessary to the module

randInt(min:int, max:int):int
this function is the default random number generator used by the coupon generator
it does not provide seeding capabilities
it can be safely overwriten by another function with the same signature

randCoupon(chars?:table):string
this function generates a random coupon under the form of a string
if its argument is null or undefined it will fall back to the default chars table

loadCoupons(path:string, reviver?:(key: any, value: any) => any, format?:string):object
this function attempts to read a json file as a coupon list
it takes a valid path, and optionnal arguments regarding the json parsing (reviver)
and the format of the file
in case of success, it returns a dictionnary contaning the coupon list
in case of error, it returns null

saveCoupons(path:string, coupons:object, replacer?:(key: string, value: any) => any, space?:int, format?:string):bool
saves a list of coupons to a file
it takes a valid path, a coupon dictionnary and optionnal arguments regarding the json (replacer, space)
and the format of the file
it returns true in case of success and false in case of error

newCoupon(path:string, ammount:int,
    chars?:table, overwrite?:bool, code?:string, generator?:function,
    replacer?:(key: string, value: any) => any, space?:int, format?:string):string


je finirais la doc un jour si j'ai le courage
*/